/*global parkitApp, Backbone*/

parkitApp.Routers = parkitApp.Routers || {};

(function () {
	'use strict';

	parkitApp.Routers.ParqueaderosRouter = Backbone.Router.extend({
		
		routes:{
			'parqueaderos'								:			'parqueaderos',
			'parqueaderos/new'						:			'nuevoParqueadero',
			'parqueaderos/:id'						:			'verParqueadero',
			'parqueaderos/edit/:id'				:			'editParqueadero'
		},

		parqueaderos: function () {
			// console.log('Routed: Parqueaderos');

			parkitApp.Views.parqueaderosView.delegateEvents();
			parkitApp.Views.parqueaderosView.render();
		},
		nuevoParqueadero: function () {
			// console.log('Routed: Nuevo Parqueadero');
			parkitApp.Views.newParqueaderoView.render();
		},
		verParqueadero: function (id) {
			// console.log('Routed: Parqueadero');
			var parqueaderoModel = parkitApp.Collections.parqueaderosCollection.findWhere({id: id});
			parkitApp.Views.parqueaderoView.render(parqueaderoModel);
		},
		editParqueadero: function (id) {
			// console.log('Routed: Edit Parqueadero');
			var parqueaderoModel = parkitApp.Collections.parqueaderosCollection.findWhere({id: id});
			parkitApp.Views.editParqueaderoView.render(parqueaderoModel);			
		}

	});

})();
