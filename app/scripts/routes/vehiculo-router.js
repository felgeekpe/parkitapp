/*global parkitApp, Backbone*/

parkitApp.Routers = parkitApp.Routers || {};

(function () {
	'use strict';

	parkitApp.Routers.VehiculosRouter = Backbone.Router.extend({
		
		routes:{
			'vehiculos'					:				'vehiculos',
			'vehiculos/:id'			:				'verVehiculo'
		},

		vehiculos: function () {
			// console.log('Routed: Vehiculos');
			parkitApp.Views.vehiculosView.render();
		},
		verVehiculo: function (id) {
			// console.log('Routed: Ver Vehiculo');
			var clientModel = parkitApp.Collections.vehiculosCollection.findWhere({id: id});
			// parkitApp.Views.vehiculoView.delegateEvents();
			parkitApp.Views.vehiculoView.render(clientModel);
		}

	});

})();
