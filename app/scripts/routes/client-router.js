/*global parkitApp, Backbone*/

parkitApp.Routers = parkitApp.Routers || {};

(function () {
	'use strict';

	parkitApp.Routers.ClientRouter = Backbone.Router.extend({
		
		routes:{
			'clientes'						:			'clientes',
			'clientes/new'				:			'nuevoCliente',
			'clientes/:id'				:			'verCliente',
			'clientes/edit/:id'		:			'editarCliente'
		},

		clientes: function () {
			// console.log('Routed: Clientes');
			parkitApp.Views.clientsView.delegateEvents();
			parkitApp.Views.clientsView.render();
		},
		
		nuevoCliente: function () {
			// console.log('Routed: Nuevo cliente');
			parkitApp.Views.newClientView.render();
		},

		verCliente: function (id) {
			// console.log('Routed: Ver cliente');
			var clientModel = parkitApp.Collections.clientsCollection.findWhere({id: id});
			parkitApp.Views.clientView.delegateEvents();
			parkitApp.Views.clientView.render(clientModel);
		},

		editarCliente: function (id) {
			// console.log('Routed: Editar cliente');
			var clientModel = parkitApp.Collections.clientsCollection.findWhere({id: id});
			parkitApp.Views.editClientView.render(clientModel);
		}

	});

})();
