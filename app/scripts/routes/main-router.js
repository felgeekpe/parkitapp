/*global parkitApp, Backbone*/

parkitApp.Routers = parkitApp.Routers || {};

(function () {
	'use strict';

	parkitApp.Routers.MainRouter = Backbone.Router.extend({
		
		routes:{
			''			:			'home'
		},

		home: function () {
			// console.log('Routed: Home');
			parkitApp.Views.homeView.render();
		}

	});

})();
