/*global parkitApp, Backbone*/

parkitApp.Models = parkitApp.Models || {};

(function () {
	'use strict';

	parkitApp.Models.ClientModel = Backbone.Model.extend({

		defaults: {
			nombres		: '',
			apellidos	: '',
			cedula			: '',
			puntos			: 0,
			vehiculoId	:	''
		},
		
		getFullName: function () {
			return this.get('nombres') + ' ' + this.get('apellidos');
		},

		nivel: function  () {
			return this.get('puntos') > 300 ? 'blue' : 'green';
		}
	});

})();
