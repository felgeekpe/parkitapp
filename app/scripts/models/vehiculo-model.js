/*global parkitApp, Backbone*/

parkitApp.Models = parkitApp.Models || {};

(function () {
    'use strict';

    parkitApp.Models.VehiculoModel = Backbone.Model.extend({

        defaults: {
            tipo: '',
            placa: '',
            marca: '',
            modelo: '',
            color: '',
            asignado: true
        }
    });

})();
