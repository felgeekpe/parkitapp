/*global parkitApp, Backbone*/

parkitApp.Models = parkitApp.Models || {};

(function () {
    'use strict';

    parkitApp.Models.ParqueaderoModel = Backbone.Model.extend({

        url: '',

        defaults: {
            nombre: '',
            latitude: '',
            longitude: '',
            capacidad: 0,
            tarifa: {
                moto: 0,
                carro: 0
            },
            horario: {
                d0: {
                    from: '',
                    to: '',
                    nombreDia: 'domingo'
                },
                d1: {
                    from: '',
                    to: '',
                    nombreDia: 'lunes'
                },
                d2: {
                    from: '',
                    to: '',
                    nombreDia: 'martes'
                },
                d3: {
                    from: '',
                    to: '',
                    nombreDia: 'miercoles'
                },
                d4: {
                    from: '',
                    to: '',
                    nombreDia: 'jueves'
                },
                d5: {
                    from: '',
                    to: '',
                    nombreDia: 'viernes'
                },
                d6: {
                    from: '',
                    to: '',
                    nombreDia: 'sabado'
                }
            }

        }
    });

})();
