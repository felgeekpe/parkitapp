/*global parkitApp, $*/


window.parkitApp = {
	Models: {},
	Collections: {},
	Views: {},
	Routers: {},
	userLocation: {
		lat: 4.598056000000001, 
		lon: -74.075833
	},
	init: function () {
		'use strict';
		// console.log('Hello from parkitApp!');
						


	/* ====== COLLECTIONS ====== */
		this.Collections.clientsCollection		=	new	this.Collections.ClientsCollection();
		this.Collections.vehiculosCollection	=	new	this.Collections.VehiculosCollection();
		this.Collections.parqueaderosCollection	=	new	this.Collections.ParqueaderosCollection();

	/* ====== VIEWS ====== */
		/* ====== HOME ====== */
		this.Views.homeView								=	new	this.Views.HomeView();

		/* ====== PARQUEADEROS ====== */
		this.Views.parqueaderosView				=	new	this.Views.ParqueaderosView();
		this.Views.parqueaderoView				=	new	this.Views.ParqueaderoView();
		this.Views.newParqueaderoView			=	new	this.Views.NewParqueaderoView();
		this.Views.editParqueaderoView		=	new	this.Views.EditParqueaderoView();

		/* ====== CLIENTS ====== */
		this.Views.clientsView						=	new	this.Views.ClientsView();
		this.Views.clientView							=	new	this.Views.ClientView();
		this.Views.newClientView					=	new	this.Views.NewClientView();
		this.Views.editClientView					=	new	this.Views.EditClientView();

		/* ====== VEHICULOS ====== */
		this.Views.vehiculosView					=	new	this.Views.VehiculosView();
		this.Views.vehiculoView						=	new	this.Views.VehiculoView();

	/* ====== ROUTERS ====== */
		this.Routers.mainRouter						=	new	this.Routers.MainRouter();
		this.Routers.parqueaderosRouter		=	new	this.Routers.ParqueaderosRouter();
		this.Routers.vehiculosRouter			=	new	this.Routers.VehiculosRouter();
		this.Routers.clientRouter					=	new	this.Routers.ClientRouter();
		Backbone.history.start();
	}
};

$(document).ready(function () {
	'use strict';

	
	parkitApp.init();

	$('body').delegate('.go-back', 'click', function(event) {
		window.history.back();
	});

	L.Icon.Default.imagePath = 'images/'

});