/*global parkitApp, Backbone*/

parkitApp.Collections = parkitApp.Collections || {};

(function () {
	'use strict';

	parkitApp.Collections.ClientsCollection = Backbone.Collection.extend({

		model: parkitApp.Models.ClientModel,
		
		localStorage: new Backbone.LocalStorage('ClientsCollection'),
		
		initialize: function () {
			this.fetch();

			this.listenTo(this, 'add', this.addOne);
			this.listenTo(this, 'change', this.changeOne);
			this.listenTo(this, 'destroy', this.removeOne);
		},
		addOne: function (model) {
			// console.log(model.getFullName() + ' has been created');
			parkitApp.Routers.clientRouter.navigate('#/clientes');
		},
		changeOne: function (model) {
			// console.log(model.getFullName() + ' has been updated');
			parkitApp.Routers.clientRouter.navigate('#/clientes');
		},
		removeOne: function (model) {
			// console.log(model.getFullName() + ' has been removed');
			parkitApp.Routers.clientRouter.navigate('#/clientes');
		}

	});

})();
