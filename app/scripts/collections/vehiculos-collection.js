/*global parkitApp, Backbone*/

parkitApp.Collections = parkitApp.Collections || {};

(function () {
    'use strict';

    parkitApp.Collections.VehiculosCollection = Backbone.Collection.extend({

        model: parkitApp.Models.VehiculoModel,

        localStorage: new Backbone.LocalStorage('VehiculosCollection'),

        initialize: function () {
            this.fetch();
            this.listenTo(this, 'add', this.addOne);
            this.listenTo(this, 'change', this.changeOne);
            this.listenTo(this, 'destroy', this.removeOne);
        },
        addOne: function (model) {
            // console.log(model.get('placa') + ' has been created');
            parkitApp.Routers.clientRouter.navigate('#/clientes');
        },
        changeOne: function (model) {
            // console.log(model.get('placa') + ' has been updated');
            parkitApp.Routers.clientRouter.navigate('#/clientes');
        },
        removeOne: function (model) {
            // console.log(model.get('placa') + ' has been removed');
            parkitApp.Routers.clientRouter.navigate('#/clientes');
        }
    });

})();
