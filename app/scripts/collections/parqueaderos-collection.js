/*global parkitApp, Backbone*/

parkitApp.Collections = parkitApp.Collections || {};

(function () {
    'use strict';

    parkitApp.Collections.ParqueaderosCollection = Backbone.Collection.extend({

        model: parkitApp.Models.ParqueaderoModel,

        localStorage: new Backbone.LocalStorage('ParqueaderosCollection'),

        initialize: function () {
          this.fetch();

          this.listenTo(this, 'add', this.addOne);
          this.listenTo(this, 'change', this.changeOne);
          this.listenTo(this, 'destroy', this.removeOne);
        },
        addOne: function (model) {
          // console.log('Parqueadero' + model.get('nombre') + ' has been created');
          parkitApp.Routers.clientRouter.navigate('#/parqueaderos');
        },
        changeOne: function (model) {
          // console.log('Parqueadero' + model.get('nombre') + ' has been updated');
          parkitApp.Routers.clientRouter.navigate('#/parqueaderos');
        },
        removeOne: function (model) {
          // console.log('Parqueadero' + model.get('nombre') + ' has been removed');
          parkitApp.Routers.clientRouter.navigate('#/parqueaderos');
        }

    });

})();
