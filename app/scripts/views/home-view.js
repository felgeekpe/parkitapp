/*global parkitApp, Backbone, JST*/

parkitApp.Views = parkitApp.Views || {};

(function () {
    'use strict';

    parkitApp.Views.HomeView = Backbone.View.extend({

        template: JST['app/scripts/templates/home-view.ejs'],

        el: '#main-container',

        render: function () {
            this.$el.html(this.template());
        }

    });

})();
