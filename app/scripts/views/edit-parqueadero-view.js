/*global parkitApp, Backbone, JST*/

parkitApp.Views = parkitApp.Views || {};

(function () {
    'use strict';

    parkitApp.Views.EditParqueaderoView = Backbone.View.extend({

        template: JST['app/scripts/templates/edit-parqueadero-view.ejs'],

        el: '#main-container',

        events: {
            'click #save-edit-parqueadero'      :       'editarParqueadero'
        },

        render: function (model) {
            this.model = model;
            this.$el.html(this.template({ parqueadero: this.model.toJSON() }));
            var latlng = [this.model.toJSON().latitude, this.model.toJSON().longitude];

            this.createMap(latlng);

            parkitApp.map.marker = new L.marker(latlng);
            parkitApp.map.addLayer(parkitApp.map.marker);

            parkitApp.map.on('dblclick', function(e) {
                if(parkitApp.map.hasLayer(parkitApp.map.marker)){
                    parkitApp.map.removeLayer(parkitApp.map.marker);
                }
                parkitApp.map.marker = L.marker([e.latlng.lat,e.latlng.lng]);
                parkitApp.map.addLayer(parkitApp.map.marker);
                $("#edit-parqueadero-form input[name='latitude']").val(e.latlng.lat);
                $("#edit-parqueadero-form input[name='longitude']").val(e.latlng.lng);
            });
        },
        editarParqueadero: function (e) {
            e.preventDefault();
            $('#edit-parqueadero-form input[name="longitude"]').removeAttr('disabled');
            $('#edit-parqueadero-form input[name="latitude"]').removeAttr('disabled');

            var data = $('#edit-parqueadero-form').serializeJSON();
            console.log(data);

            this.model.set({
                nombre: data.nombre,
                latitude: data.latitude,
                longitude: data.longitude,
                capacidad: data.capacidad,
                tarifa: data.tarifa,
                horario: data.horario
            }).save();
        },
        createMap: function (latlng) {
            parkitApp.map = L.map('map-edit-parqueadero', {
                center: latlng,
                zoom:12
            });
            L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png').addTo(parkitApp.map);
        }

    });

})();
