/*global parkitApp, Backbone, JST*/

parkitApp.Views = parkitApp.Views || {};

(function () {
	'use strict';

	parkitApp.Views.ClientsView = Backbone.View.extend({
		
		tagName: 'div',
		id: 'clients-list-table',
		template: JST['app/scripts/templates/clients-view.ejs'],
		
		events: {
			'click .view-client'        :       'verCliente'
		},

		initialize: function () {
			this.collection = parkitApp.Collections.clientsCollection;
		},
		render: function () {
			this.$el.html(this.template( { clientes: this.collection.toJSON() } ) );
			$('#main-container').html(this.$el);
		},
		verCliente: function (e) {
			var clientId = $(e.currentTarget).data('client-id');
			parkitApp.Routers.clientRouter.navigate('#/clientes/'+ clientId );
		}
	});
	
})();
