/*global parkitApp, Backbone, JST*/

parkitApp.Views = parkitApp.Views || {};

(function () {
	'use strict';

	parkitApp.Views.EditClientView = Backbone.View.extend({

		template: JST['app/scripts/templates/edit-client-view.ejs'],
		
		el: '#main-container',
		events: {
			'click #save-edit-client'           :  'editarCliente'
		},

		initialize: function () {
		},

		render: function (model) {
			this.model = model;
			this.vehiculo = parkitApp.Collections.vehiculosCollection.findWhere({id: this.model.get('vehiculoId')});
			var options = {
				cliente: this.model.toJSON(),
				vehiculo: this.vehiculo.toJSON(),
			};
			
			this.$el.html(this.template(options));
		},
		editarCliente: function (e) {
			e.preventDefault();
			var clientData = $('#edit-client-form').serializeJSON();
			var vehiculoData = $('#edit-vehiculo-form').serializeJSON();

			this.model.set({
				nombres: clientData.nombres,
				apellidos: clientData.apellidos,
				cedula: clientData.cedula
			}).save();

			this.vehiculo.set({
				tipo: vehiculoData.tipo,
				placa: vehiculoData.placa,
				marca: vehiculoData.marca,
				modelo: vehiculoData.modelo,
				color: vehiculoData.color
			}).save();
		}
	});

})();
