/*global parkitApp, Backbone, JST*/

parkitApp.Views = parkitApp.Views || {};

(function () {
    'use strict';

    parkitApp.Views.ParqueaderoView = Backbone.View.extend({

        template: JST['app/scripts/templates/parqueadero-view.ejs'],

        el: '#main-container',

        events: {
            'click .edit-parqueadero'           :       'editarParqueadero',
            'click .delete-parqueadero'       :       'eliminarParqueadero'
        },

        render: function (model) {
            this.model = model;
            this.$el.html(this.template({parqueadero: this.model.toJSON() }));
            var latlng = [this.model.toJSON().latitude, this.model.toJSON().longitude];


            this.createMap(latlng);

            parkitApp.map.marker = new L.marker(latlng);
            parkitApp.map.addLayer(parkitApp.map.marker);

        },
        editarParqueadero: function (e) {
            var parqueaderoId = $(e.currentTarget).data('parqueadero-id');
            parkitApp.Routers.parqueaderosRouter.navigate('#/parqueaderos/edit/'+ parqueaderoId );
        },
        eliminarParqueadero: function (e) {
            e.preventDefault();
            var parqueaderoId = $(e.currentTarget).data('parqueadero-id');
            var parqueaderoModel = parkitApp.Collections.parqueaderosCollection.findWhere({id: parqueaderoId});
            
            if (window.confirm('Intenta eliminar al Parqueadero: \n\n' + parqueaderoModel.get('nombre') + '\n\nContinuar?')) {
                parqueaderoModel.destroy();
            } else {
                parkitApp.Routers.parqueaderosRouter.navigate('#/parqueaderos');
            }
        },
        createMap: function (latlng) {
            parkitApp.map = L.map('map-parqueadero', {
                center: latlng,
                zoom:15,
                dragging: false,
                doubleClickZoom: false,
                touchZoom: false,
                scrollWheelZoom: false,
                boxZoom: false,
                tap: false
            });
            L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png').addTo(parkitApp.map);
        }
    });

})();
