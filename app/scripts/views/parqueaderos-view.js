/*global parkitApp, Backbone, JST*/

parkitApp.Views = parkitApp.Views || {};

(function () {
    'use strict';

    parkitApp.Views.ParqueaderosView = Backbone.View.extend({

        template: JST['app/scripts/templates/parqueaderos-view.ejs'],

        el: '#main-container',

        events: {
            'click .ver-lista'          :   'verLista',
            'click .ver-mapa'           :   'verMapa',
            'click .ver-parqueadero'    :   'verParqueadero'
        },

        render: function () {
            this.collection = parkitApp.Collections.parqueaderosCollection;

            this.$el.html(this.template({parqueaderos: this.collection.toJSON() }));

            this.createMap();

            if( parkitApp.map.hasLayer(parkitApp.map.listMarkers) ){
                parkitApp.map.removeLayer(parkitApp.map.listMarkers);
            }

            parkitApp.map.listMarkers = [];
            _.each(this.collection.toJSON(), function(parqueadero, element){
                parkitApp.map.listMarkers.push( new L.marker([parqueadero.latitude,parqueadero.longitude]) );
                var popupString = '<strong>Nombre: </strong>' + parqueadero.nombre + '<br><strong>Capacidad: </strong>' + parqueadero.capacidad;
                parkitApp.map.addLayer(parkitApp.map.listMarkers[element].bindPopup(popupString));
            });

        },
        verParqueadero: function (e) {
            var parqueaderoId = $(e.currentTarget).data('parqueadero-id');
            parkitApp.Routers.parqueaderosRouter.navigate('#/parqueaderos/' + parqueaderoId);
        },
        verLista: function (event) {
            var target = $(event.currentTarget);
            target.parent().find('button').each(function (index, el) {
                $(el).removeClass('active');
            });
            target.addClass('active');
            $('.lista').removeClass('hidden');
            $('.mapa').addClass('hidden');
        },
        verMapa: function (event) {
            var target = $(event.currentTarget);
            target.parent().find('button').each(function (index, el) {
                $(el).removeClass('active');
            });
            target.addClass('active');
            $('.lista').addClass('hidden');
            $('.mapa').removeClass('hidden');
            // reset map size
            parkitApp.map.invalidateSize();
        },
        createMap: function () {
            parkitApp.map = L.map('map', {
                zoom:12
            });
            L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png').addTo(parkitApp.map);
            parkitApp.map.locate({setView: true});
        }

    });

})();
