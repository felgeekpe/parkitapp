/*global parkitApp, Backbone, JST*/

parkitApp.Views = parkitApp.Views || {};

(function () {
    'use strict';

    parkitApp.Views.VehiculoView = Backbone.View.extend({

        template: JST['app/scripts/templates/vehiculo-view.ejs'],

        tagName: 'div',
        className: 'container',

        events: {
            'click .edit-vehiculo' : 'editVehiculo'
        },

        render: function (model) {
            this.model = model;
            var options = {
                vehiculo: this.model
            };
            this.$el.html(this.template(options));
            $('#main-container').html(this.$el);
        },
        editVehiculo: function (e) {
            e.preventDefault();
            var vehiculoId = $(e.currentTarget).data('vehiculo-id');
            var clientModel = parkitApp.Collections.clientsCollection.findWhere({vehiculoId: vehiculoId});

            parkitApp.Routers.clientRouter.navigate('#/clientes/edit/' + clientModel.get('id'));
        }

    });

})();
