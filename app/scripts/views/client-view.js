/*global parkitApp, Backbone, JST*/

parkitApp.Views = parkitApp.Views || {};

(function () {
	'use strict';

	parkitApp.Views.ClientView = Backbone.View.extend({

		tagName: 'div',
		className: 'container',
		template: JST['app/scripts/templates/client-view.ejs'],
		
		events: {
			'click .edit-client'        :       'editarCliente',
			'click .delete-client'      :       'eliminarCliente'
		},

		render: function (model) {
			this.model = model;
			this.vehiculo = parkitApp.Collections.vehiculosCollection.findWhere({id: this.model.get('vehiculoId') });
			var options = {
				cliente: this.model,
				vehiculo: this.vehiculo
			};

			this.$el.html(this.template(options));
			$('#main-container').html(this.$el);
		},
		editarCliente: function (e) {
			var clientId = $(e.currentTarget).data('client-id');
			parkitApp.Routers.clientRouter.navigate('#/clientes/edit/'+ clientId );
		},
		eliminarCliente: function (e) {
			e.preventDefault();
			var clientId = $(e.currentTarget).data('client-id');
			var clientModel = parkitApp.Collections.clientsCollection.findWhere({id: clientId});
			var vehiculoModel = parkitApp.Collections.vehiculosCollection.findWhere({id: clientModel.get('vehiculoId')});
			
			if (window.confirm('Intenta eliminar al cliente: \n\n' + clientModel.getFullName() + '\n\nContinuar?')) {
				clientModel.destroy();
				vehiculoModel.destroy();
			} else {
				parkitApp.Routers.clientRouter.navigate('#/clientes');
			}
		}
	});

})();
