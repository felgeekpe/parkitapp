/*global parkitApp, Backbone, JST*/

parkitApp.Views = parkitApp.Views || {};

(function () {
	'use strict';

	parkitApp.Views.NewClientView = Backbone.View.extend({

		template: JST['app/scripts/templates/new-client-view.ejs'],
		el: '#main-container',
		events: {
			'click #save-new-client'             :       'crearCliente'
		},

		model: {},

		render: function () {
			this.$el.html(this.template());
		},

		crearCliente: function (e) {
			e.preventDefault();
			var vehiculoData = $('#new-vehiculo-form').serializeJSON();
			var clientData = $('#new-client-form').serializeJSON();

			var vehiculoModel = parkitApp.Collections.vehiculosCollection.create(vehiculoData);
			
			clientData.vehiculoId = vehiculoModel.id;
			parkitApp.Collections.clientsCollection.create(clientData);
		}
	});

})();
