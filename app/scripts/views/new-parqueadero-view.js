/*global parkitApp, Backbone, JST*/

parkitApp.Views = parkitApp.Views || {};

(function () {
    'use strict';

    parkitApp.Views.NewParqueaderoView = Backbone.View.extend({

        template: JST['app/scripts/templates/new-parqueadero-view.ejs'],

        tagName: 'div',
        el: '#main-container',
        className: '',

        events: {
            'click #save-new-parqueadero'  : 'nuevoParqueadero'
        },

        render: function () {
            this.$el.html(this.template());

            parkitApp.map = L.map('map-new-parqueadero', {
                zoom:12,
            });
            L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png').addTo(parkitApp.map);
            parkitApp.map.locate({setView: true});
            
            parkitApp.map.on('dblclick', function(e) {
                if(parkitApp.map.hasLayer(parkitApp.map.marker)){
                    parkitApp.map.removeLayer(parkitApp.map.marker);
                }
                parkitApp.map.marker = L.marker([e.latlng.lat,e.latlng.lng]);
                parkitApp.map.addLayer(parkitApp.map.marker);
                $("#new-parqueadero-form input[name='latitude']").val(e.latlng.lat);
                $("#new-parqueadero-form input[name='longitude']").val(e.latlng.lng);
            });
        },

        nuevoParqueadero: function (e) {
            e.preventDefault();
            $('#new-parqueadero-form input[name="longitude"]').removeAttr('disabled');
            $('#new-parqueadero-form input[name="latitude"]').removeAttr('disabled');
            var data = $('#new-parqueadero-form').serializeJSON();
            parkitApp.Collections.parqueaderosCollection.create(data);
        }

    });

})();
