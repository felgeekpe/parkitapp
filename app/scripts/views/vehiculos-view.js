/*global parkitApp, Backbone, JST*/

parkitApp.Views = parkitApp.Views || {};

(function () {
    'use strict';

    parkitApp.Views.VehiculosView = Backbone.View.extend({

        template: JST['app/scripts/templates/vehiculos-view.ejs'],

        el: '#main-container',

        events: {
            'click .view-vehiculo'        :       'verVehiculo'
        },

        initialize: function () {
            this.collection = parkitApp.Collections.vehiculosCollection; 
        },
        render: function () {
            this.$el.html(this.template({vehiculos: this.collection.toJSON()}));
        },
        verVehiculo: function (e) {
            var vehiculoId = $(e.currentTarget).data('client-id');
            parkitApp.Routers.vehiculosRouter.navigate('#/vehiculos/'+ vehiculoId );
        }

    });

})();
